const EWS = require('node-ews');

//disable https verification (not advisable)
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
 
// exchange server connection info
const ewsConfig =  require('./.ews-config.json');
 
// initialize node-ews
const ews = new EWS(ewsConfig);
 
// define ews api function = maillisten expanden
const ewsFunction = 'ExpandDL';
 
// define ews api function args
const ewsArgs = {
  'Mailbox': {
    'EmailAddress':''
  }
};

function listcontacts(arg1, arg2) {

  console.log("listcontacts called with:" +  arg1 + ":" + arg2);
  var maillist = [];
  
  
  return pump(arg1,arg2);

  function pump(arg1, arg2){

    console.log(" pump called");

    return ews.run(arg1, arg2)
      .then(result => {
       
      var promises = [];
      console.log("ews run called (async web call) with: " + JSON.stringify(arg2)); 

      var items = result.ResponseMessages.ExpandDLResponseMessage.DLExpansion.Mailbox; 
      if (result.ResponseMessages.ExpandDLResponseMessage.DLExpansion.attributes.TotalItemsInView != 0){
        promises.push(items.forEach(function(item){
              var x = item.MailboxType;
              console.log(item.EmailAddress +  ": " + x);

              //if the Listitem is a PublicDistributionList itself prepare for recursive call to self 
              if (x == 'PublicDL'){
                newewsArgs = {
                    'Mailbox': {
                    'EmailAddress': item.EmailAddress        
                    }
              };
                console.log("found new maillist in response: " + item.EmailAddress );               
                  return pump('ExpandDL',newewsArgs);
                //recursively execute function to get adresses
              } else{
                //gewoon een mailadres
                var x = item.MailboxType;
                maillist.push(item.EmailAddress);
              } 
                 
            }));
            return Promise.all(promises).then(() => 
              console.log("Maillist scanned")
              );  
          }else{
            console.log("empty");
          }
        
      })
      .catch(err => {
        console.log(err.message);
      });
    }
    
}

// query EWS and print resulting JSON to console
listcontacts(ewsFunction, ewsArgs)
.then(function(){
  console.log("eind");
})
 